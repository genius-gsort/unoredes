package main;

import java.io.IOException;

import http.Mensagem;
import http.SendMessage;
import http.TipoMensagem;
import models.Carta;
import models.Cores;

public class Main {
	
	public static void main(String[] args) {
		Mensagem mensagem = new Mensagem(TipoMensagem.CONECTAR, "Luan", null);
		try {
			SendMessage.client("localhost", mensagem);
		} catch (IOException e) {
			System.out.println("Erro ao enviar mensagem ao servidor");
		}
	}
}
