package http;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class SendMessage {
	private static int SERVER_PORT = 3000;
	
	public static void client(String ipServer, Mensagem mensagem) throws IOException {
		Socket client = new Socket(ipServer, SERVER_PORT);
		if(mensagem != null) {
			sendMessage(client, mensagem);
		}
	}
	
	private static void sendMessage(Socket client, Mensagem mensagem) throws IOException {
		ObjectOutputStream output = new ObjectOutputStream(client.getOutputStream());
		output.writeObject(mensagem);
		output.close();
	}
}
