package http;

import java.io.Serializable;

import models.Carta;

public class Mensagem implements Serializable{
	
	private TipoMensagem tipo;
	private String nome;
	private Carta carta;
	
	public Mensagem(TipoMensagem tipo, String nome, Carta carta) {
		this.tipo = tipo;
		this.nome = nome;
		this.carta = carta;
	}
	
	public Mensagem (TipoMensagem tipo, String nome) {
		this.tipo = tipo;
		this.nome = nome;
		this.carta = null;
	}

	public TipoMensagem getTipo() {
		return tipo;
	}
	
	public String getNome() {
		return nome;
	}

	public Carta getCarta() {
		return carta;
	}
}
