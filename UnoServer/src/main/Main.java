package main;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import http.Request;
import models.Partida;

public class Main {

	private static int SERVER_PORT = 3000;
	public static Partida partida;
	
	public static void main(String[] args) throws IOException {
	    System.out.println("Servidor Web nor ar rodando na porta " + SERVER_PORT + "...");
	    ServerSocket server= new ServerSocket(SERVER_PORT);
	    partida = new Partida();
	    
	    Thread thread;
	    
	    while (true) {
	      Socket client = server.accept();
	      Request request = new Request(client);
	      
	      thread = new Thread(request);	      
	      thread.start();
	    }

	}

}
