package models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Partida {

	private	Map<Integer, Jogador> jogadores;
	private List<Carta> monte;
	private List<Jogador> vencedores;
	private Integer jogadorAtual;
	private Carta topo;
	
	public Partida() {
		this.jogadores = new HashMap<Integer, Jogador>();
		this.monte = new ArrayList<Carta>();
		this.topo = null;
		this.jogadorAtual = 1;
	}
	
	public Partida(Map<Integer, Jogador> jogadores) {
		super();
		this.jogadores = jogadores;
		this.monte = null;
		this.topo = null;
		this.jogadorAtual = 1;
	}

	public Map<Integer, Jogador> getJogadores() {
		return jogadores;
	}

	public void setJogadores(Map<Integer, Jogador> jogadores) {
		this.jogadores = jogadores;
	}

	public List<Carta> getMonte() {
		return monte;
	}

	public void setMonte(List<Carta> monte) {
		this.monte = monte;
	}

	public Carta getTopo() {
		return topo;
	}

	public void setTopo(Carta topo) {
		this.topo = topo;
	}
	
	public Integer getJogadorAtual() {
		return jogadorAtual;
	}
	
	public void addJogador(Integer ordem, Jogador jogador) {
		jogadores.put(ordem, jogador);
	}
	
	public void removeJogador(Integer ordem) {
		jogadores.remove(ordem);
	}
	
	public void addVencedor(Jogador jogador) {
		vencedores.add(jogador);
	}
	
	public List<Jogador> getVencedores(){
		return this.vencedores;
	}
	
	public void proximoJogador() {
		Integer totalJogadores = 0;
		
		// Descobre o total de jogadores ativos
		for(Jogador jogador : jogadores.values()) {
			if(jogador.temCartas()) {
				totalJogadores++;
			}
		}
		
		do {
			// Se o jogador atual for o último da ordem, volta para o começo
			if(jogadorAtual.equals(totalJogadores)) {
				jogadorAtual = 0;
			}
			jogadorAtual++;
		} while(jogadores.get(jogadorAtual).temCartas() == false);
	}
}
