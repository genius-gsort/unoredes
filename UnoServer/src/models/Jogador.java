package models;

import java.net.Socket;

public class Jogador {
	
	private String nome;
	private boolean temCartas;
	private Socket socket;
	
	public Jogador(String nome, Socket socket) {
		this.nome = nome;
		this.socket = socket;
		this.temCartas = true;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Socket getSocket() {
		return socket;
	}

	public void setSocket(Socket socket) {
		this.socket = socket;
	}
	
	public boolean temCartas() {
		return temCartas;
	}

	public void setTemCartas(boolean temCartas) {
		this.temCartas = temCartas;
	}
}
