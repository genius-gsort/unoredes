package models;

import java.io.Serializable;

public class Carta implements Serializable{

	private Integer valor;
	private Cores cor;
	
	public Carta(Integer valor, Cores cor) {
		this.valor = valor;
		this.cor = cor;
	}

	public int getValor() {
		return valor;
	}

	public void setValor(Integer valor) {
		this.valor = valor;
	}

	public Cores getCor() {
		return cor;
	}

	public void setCor(Cores cor) {
		this.cor = cor;
	}
}
