package http;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class SendMessage {
	public static void client(Socket client, Mensagem mensagem) throws IOException {
		if(mensagem != null) {
			sendMessage(client, mensagem);
		}
	}
	
	private static void sendMessage(Socket client, Mensagem mensagem) throws IOException {
		ObjectOutputStream output = new ObjectOutputStream(client.getOutputStream());
		output.writeObject(mensagem);
		output.close();
	}
}
