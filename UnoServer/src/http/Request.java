package http;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;

import exceptions.UsuarioJaConectadoException;
import main.Main;
import models.Jogador;
import models.Partida;

public class Request implements Runnable {
	
	private Socket client;
	private Partida partidaAtual = Main.partida;
	
	public Request(Socket client) {
		this.client = client;
	}
	
	private void operacaoRequest() throws IOException, ClassNotFoundException, UsuarioJaConectadoException {
		ObjectInputStream input = new ObjectInputStream(this.client.getInputStream());
		Mensagem mensagem = (Mensagem) input.readObject();
		
		switch(mensagem.tipo()) {
			case CONECTAR: adicionarJogador(mensagem); break;
			case JOGADA: fazerJogada(mensagem); break;
		}
	}
	
	private void enviarMensagemParaTodosJogadores(Mensagem mensagem) throws IOException {
		Socket destinatario;
		
		for (var jogador : partidaAtual.getJogadores().entrySet()) {
			try {
				destinatario = jogador.getValue().getSocket();
				SendMessage.client(destinatario, mensagem);
			} catch (Exception e) {
				partidaAtual.getJogadores().remove(jogador.getKey());
			}
		}
	}
	
	private void jogadorTerminou() {
		
	}

	private void fazerJogada(Mensagem mensagem) throws IOException {
		if(mensagem.texto() != null) {
			jogadorTerminou();
		}
		Mensagem mudarTopo = new Mensagem(TipoMensagem.NOVO_TOPO, mensagem.carta());
		enviarMensagemParaTodosJogadores(mudarTopo);
		
		Socket proximo = proximoJogador();
		Mensagem permitirJogada = new Mensagem(TipoMensagem.PODE_JOGAR);
		
		SendMessage.client(proximo, permitirJogada);
	}
	
	private void encerrarPartida(Mensagem mensagem) throws IOException {
		Mensagem jogoEncerrado = new Mensagem(TipoMensagem.JOGO_ENCERRADO, mensagem.texto());
		enviarMensagemParaTodosJogadores(jogoEncerrado);
		partidaAtual = new Partida();
	}
	
	private Socket proximoJogador(){
		partidaAtual.proximoJogador();
		Integer jogadorAtual = partidaAtual.getJogadorAtual();
		return partidaAtual.getJogadores().get(jogadorAtual).getSocket();
	}

	private void adicionarJogador(Mensagem mensagem) throws UsuarioJaConectadoException {
		String ipClient = client.getInetAddress().getCanonicalHostName();
		
		for(Jogador jogador : partidaAtual.getJogadores().values()) {
			String ipRegistrado = jogador.getSocket().getInetAddress().getCanonicalHostName();
			if(ipRegistrado.equalsIgnoreCase(ipClient)) {
				throw new UsuarioJaConectadoException();
			}
		}
		
		Integer ordem = partidaAtual.getJogadores().size() + 1;
		partidaAtual.getJogadores().put(ordem, new Jogador(mensagem.texto(), client));
	}
	
	@Override
	public void run() {
		try {
			operacaoRequest();
		} catch (ClassNotFoundException e) {
			System.out.println("Mensagem enviada inválida inválida");
		} catch (IOException e) {
			System.out.println("Erro de IO");
		} catch (UsuarioJaConectadoException e) {
			System.out.println("Usuário já está na sala");
		}
		
		for(Integer ordem : partidaAtual.getJogadores().keySet()) {
			System.out.println("Jogador "+ ordem + ": " + partidaAtual.getJogadores().get(ordem).getNome() + " " + partidaAtual.getJogadores().get(ordem).getSocket().getInetAddress().getCanonicalHostName());
			
		}
		System.out.println("\n\n\n");
		
	}

}
