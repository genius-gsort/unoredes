package http;

public enum TipoMensagem {
	
	CONECTAR,
	JOGADA,
	PODE_JOGAR,
	TERMINOU,
	NOVO_TOPO,
	JOGO_ENCERRADO
}
