package http;

import java.io.Serializable;

import models.Carta;

public record Mensagem(
		TipoMensagem tipo,
		String texto,
		Carta carta) implements Serializable {

	public Mensagem(TipoMensagem tipo) {
		this(tipo,null,null);
	}
	
	public Mensagem(TipoMensagem tipo, String texto) {
		this(tipo, texto, null);
	}
	
	public Mensagem(TipoMensagem tipo, Carta carta) {
		this(tipo, null, carta);
	}

}
